Notebook_GRAIL
==============

Presentation Materials for my work on the GRAIL data in order to enhance the presence of gravity anomalies.

# Presentation Index

* Introduction
  - [Introduction.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Introduction.ipynb?create=1)
* Discussion on the Model
  - [Model_Crust_Anomaly](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Model_Crust_Anomaly.ipynb?create=1)
  - [Model_Crust_Anomaly_2](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Model_Crust_Anomaly_2.ipynb?create=1)
* Unmodified_Craters (UC)
  - [Unmodified_Craters.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Unmodified_Craters.ipynb?create=1)
* Floor fractured craters
  - [Floor_Fractured_crater.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Floor_Fractured_crater.ipynb?create=1)
* Comparaison FFC UC
  - [Comparaison_FFC_UC.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Comparaison_FFC_UC.ipynb?create=1)
  - [Comparaison_FFC_UC_Restriction.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Comparaison_FFC_UC_Restriction.ipynb?create=1)
  - [Comparaison_FFC_UC_Ttest.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Comparaison_FFC_UC_Ttest.ipynb?create=1)
* Synthetic signal
  - [DEPTH_UC_FFC.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/DEPTH_UC_FFC.ipynb?create=1)
  - [ANO_SYNTHETIQUE_PRELIMINARY.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/ANO_SYNTHETIQUE_PRELIMINARY.ipynb?create=1)

* Definition and effect distance Maria
   - [Maria_definition.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Maria_definition.ipynb?create=1)
   - [Distance_Maria.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Distance_Maria.ipynb?create=1)
* Paper jgr
   - [Paper_Highlands.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Paper_Highlands.ipynb?create=1)
   - [Paper_Highlands_50.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Paper_Highlands_50.ipynb?create=1) 
   - [Paper_Maria.ipynb](http://nbviewer.ipython.org/github/cthorey/Notebook_GRAIL/blob/master/Paper_Maria.ipynb?create=1) 
